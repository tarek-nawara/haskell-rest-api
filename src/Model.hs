{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

-----------------------------------
-- |
-- Module: Model.hs
-- Holder for model representation
------------------------------------
module Model
  ( Bookmark(..)
  , entityDefs
  , EntityField(..)
  ) where

import           Data.Aeson
import           Data.Default.Class
import           GHC.Generics

import           Database.Persist
import           Database.Persist.Class
import           Database.Persist.TH

-- Generates our `BookmarkEntity` instance and `Bookmark` record
share
  [mkPersist sqlSettings, mkSave "entityDefs"]
  [persistLowerCase|
  Bookmark
    title String
    url   String
    deriving Show Generic
|]

instance ToJSON Bookmark

toJSON (Bookmark title url) = object ["title" .= title, "url" .= url]
