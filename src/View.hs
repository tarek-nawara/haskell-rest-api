{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
------------------------------
-- |
-- Module: View.hs
-- Export our view (`BookmarkJSON`) and two helper functions for
-- turning some JSON into a `Bookmark` record or
-- turning a `Bookmark` record into a JSON string
-------------------------------
module View
  ( BookmarkJSON(..)
  , bookmarkJSONToBookmark
  , bookmarkAsJSONLBS
  ) where

import           Data.Aeson
import           Data.ByteString
import           Data.ByteString.Char8
import           Data.ByteString.Lazy
import           Data.Default.Class
import           Data.Int
import           Data.Maybe
import           Data.Text
import           Database.Persist
import           Database.Persist.Class
import           GHC.Generics
import           Model

data BookmarkJSON = BookmarkJSON
  { bookmarkJSONTitle :: Maybe String
  , bookmarkJSONUrl   :: Maybe String
  } deriving (Show, Generic)

instance FromJSON BookmarkJSON where
  parseJSON (Object v) = BookmarkJSON <$> v .:? "title" <*> v .:? "url"

instance ToJSON BookmarkJSON where
  toJSON (BookmarkJSON title url) = object ["title" .= title, "url" .= url]

bookmarkJSONToBookmark :: BookmarkJSON -> Bookmark
bookmarkJSONToBookmark bookmarkJSON = Bookmark titleJSONToTitle urlJSONToUrl
  where
    titleJSONToTitle = fromMaybe "" $ bookmarkJSONTitle bookmarkJSON
    urlJSONToUrl = fromMaybe "" $ bookmarkJSONUrl bookmarkJSON

bookmarkAsJSONLBS :: Key Bookmark -> Bookmark -> Data.ByteString.Lazy.ByteString
bookmarkAsJSONLBS k b = encode . entityIdToJSON $ Entity k b
