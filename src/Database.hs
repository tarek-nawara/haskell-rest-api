{-# LANGUAGE OverloadedStrings #-}

-----------------------------------
-- |
-- Module: Database.hs
-- Holder for database operations
-----------------------------------
module Database
  ( dbMigration
  , getBookmarks
  , getBookmarkById
  , insertBookmark
  , updateBookmarkById
  , deleteBookmarkById
  ) where

import           Model
import           View

import           Data.ByteString
import           Data.ByteString.Char8
import           Data.ByteString.Lazy
import           Data.Int
import           Data.Text
import           System.Environment

import           Data.Aeson
import           Data.Maybe

import           Control.Monad.Logger
import           Control.Monad.Trans.Control
import           Control.Monad.Trans.Resource

import           Database.Persist
import           Database.Persist.Class
import           Database.Persist.Sqlite      as DbSql

-- Gather the database connection string from the environemt
-- If not set use the default
sqliteConnString :: IO Data.Text.Text
sqliteConnString = do
  maybeDbConnString <- lookupEnv "WEB_BOOKMARKS_DB_CONN"
  return $
    Data.Text.pack $ fromMaybe "webBookmarks_default.db" maybeDbConnString

-- Needed for each database transaction (inserting, updating, retrieval, deleting)
withDbRun :: SqlPersistT (NoLoggingT (ResourceT IO)) b -> IO b
withDbRun command = do
  connString <- sqliteConnString
  runSqlite connString command

-- This will create our web bookmarks table if it does not already exist
-- Persistent will assist with update our table schema should our model change
dbMigration :: IO ()
dbMigration =
  withDbRun $
  runMigration $ migrate entityDefs $ entityDef (Nothing :: Maybe Bookmark)

-- Helper function to convert the URL ID string to the needed 64 bit integer primary key
getBookmarkIdKey :: Maybe Data.ByteString.ByteString -> Key Bookmark
getBookmarkIdKey maybeIdBS = toSqlKey bookmarkIdInt64
  where
    bookmarkIdBS = fromMaybe ("-1" :: Data.ByteString.ByteString) maybeIdBS
    bookmarkIdInt64 = read (Data.ByteString.Char8.unpack bookmarkIdBS) :: Int64

-- Retrieves multiple bookmark rows from our table starting at `start` and up to the `limit`
getBookmarks ::
     Maybe Data.ByteString.ByteString
  -> Maybe Data.ByteString.ByteString
  -> IO [Entity Bookmark]
getBookmarks maybeLimitTo maybeOffsetBy = do
  let limitToBS = fromMaybe ("10" :: Data.ByteString.ByteString) maybeLimitTo
  let offsetBy = fromMaybe ("0" :: Data.ByteString.ByteString) maybeOffsetBy
  let limitToInt = read (Data.ByteString.Char8.unpack limitToBS) :: Int
  let offsetToInt = read (Data.ByteString.Char8.unpack offsetBy) :: Int
  withDbRun $
    DbSql.selectList
      ([] :: [Filter Bookmark])
      [LimitTo limitToInt, OffsetBy offsetToInt]

getBookmarkById ::
     Maybe Data.ByteString.ByteString -> IO (Key Bookmark, Maybe Bookmark)
getBookmarkById maybeIdBS = do
  let bookmarkIdKey = getBookmarkIdKey maybeIdBS
  maybeBookmark <- withDbRun $ DbSql.get bookmarkIdKey
  return (bookmarkIdKey, maybeBookmark)

insertBookmark :: Bookmark -> IO (Key Bookmark)
insertBookmark bookmark = withDbRun $ DbSql.insert bookmark

updateBookmarkById ::
     Maybe Data.ByteString.ByteString
  -> BookmarkJSON
  -> IO (Key Bookmark, Maybe Bookmark)
updateBookmarkById maybeIdBS bookmarkJSON = do
  let bookmarkIdKey = getBookmarkIdKey maybeIdBS
  (bookmarkKeyId, maybeBook) <- getBookmarkById maybeIdBS
  case maybeBook of
    Nothing -> return (bookmarkKeyId, Nothing)
    Just bookmark -> do
      let bookmarkUpdated =
            Bookmark
            { bookmarkTitle =
                fromMaybe
                  (bookmarkTitle bookmark)
                  (bookmarkJSONTitle bookmarkJSON)
            , bookmarkUrl =
                fromMaybe (bookmarkUrl bookmark) (bookmarkJSONUrl bookmarkJSON)
            }
      withDbRun $
        DbSql.update
          bookmarkKeyId
          [ BookmarkTitle =. bookmarkTitle bookmarkUpdated
          , BookmarkUrl   =. bookmarkUrl bookmarkUpdated
          ]
      return (bookmarkKeyId, Just bookmarkUpdated)

deleteBookmarkById ::
     Maybe Data.ByteString.ByteString -> IO (Key Bookmark, Maybe Bookmark)
deleteBookmarkById maybeIdBS = do
  let bookmarkIdKey = getBookmarkIdKey maybeIdBS
  (bookmarkKeyId, maybeBookmark) <- getBookmarkById maybeIdBS
  case maybeBookmark of
    Nothing -> return (bookmarkKeyId, Nothing)
    Just bookmark -> do
      withDbRun $ DbSql.delete bookmarkKeyId
      return (bookmarkKeyId, Just bookmark)
