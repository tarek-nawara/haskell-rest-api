{-# LANGUAGE OverloadedStrings #-}

------------------------------
-- |
-- Module: Controller.hs
-- Holder for controller logic
-------------------------------
module Controller
  ( mainRouter
  ) where

import           Control.Monad.IO.Class (MonadIO, liftIO)
import           Data.Aeson
import           Data.ByteString
import           Data.ByteString.Char8
import           Data.ByteString.Lazy
import           Data.Int
import           Data.Maybe
import           Data.Text
import           Database
import           Database.Persist
import           Database.Persist.Class
import           Model
import           Snap
import           View

mainRouter :: Snap ()
mainRouter = route [("", writeBS ""), ("bookmarks", bookmarksRouter)]

bookmarksRouter :: Snap ()
bookmarksRouter =
  route
    [ ("", method GET bookmarksRouteIndex)
    , ("", method POST bookmarksRouteCreate)
    , ("/:id", method GET bookmarksRouteShow)
    , ("/:id", method PUT bookmarksRouteUpdate)
    , ("/:id", method DELETE bookmarksRouteDelete)
    ]

bookmarksRouteIndex :: Snap ()
bookmarksRouteIndex = do
  maybeLimitTo <- getParam "limit"
  maybeOffsetBy <- getParam "start"
  bookmarks <- liftIO $ getBookmarks maybeLimitTo maybeOffsetBy
  modifyResponse $ setHeader "Content-Type" "application/json"
  writeLBS $ encode $ Prelude.map entityIdToJSON bookmarks

bookmarksRouteCreate :: Snap ()
bookmarksRouteCreate = do
  body <- readRequestBody 5000
  let bookmark = bookmarkJSONToBookmark $ parseBodyToBookmarkJSON body
  bookmarkIdKey <- liftIO $ insertBookmark bookmark
  modifyResponse $ setHeader "Content-Type" "application/json"
  respondWithBookmark 201 bookmarkIdKey bookmark

bookmarksRouteShow :: Snap ()
bookmarksRouteShow = do
  set404AndContentType
  maybeBookmarkId <- getParam "id"
  (bookmarkKeyId, maybeBookmark) <- liftIO $ getBookmarkById maybeBookmarkId
  respondWithMaybeBookmark 200 bookmarkKeyId maybeBookmark

bookmarksRouteUpdate :: Snap ()
bookmarksRouteUpdate = do
  set404AndContentType
  maybeBookmarkId <- getParam "id"
  body <- readRequestBody 50000
  let bookmarkJSON = parseBodyToBookmarkJSON body
  (bookmarkIdKey, maybeBookmark) <-
    liftIO $ updateBookmarkById maybeBookmarkId bookmarkJSON
  respondWithMaybeBookmark 200 bookmarkIdKey maybeBookmark

bookmarksRouteDelete :: Snap ()
bookmarksRouteDelete = do
  set404AndContentType
  maybeBookmarkId <- getParam "id"
  (bookmarkIdKey, maybeBookmark) <- liftIO $ deleteBookmarkById maybeBookmarkId
  respondWithMaybeBookmark 200 bookmarkIdKey maybeBookmark

set404AndContentType :: Snap ()
set404AndContentType = do
  modifyResponse $ setResponseCode 404
  modifyResponse $ setHeader "Content-Type" "application/json"

parseBodyToBookmarkJSON :: Data.ByteString.Lazy.ByteString -> BookmarkJSON
parseBodyToBookmarkJSON body =
  fromMaybe
    (BookmarkJSON (Just "") (Just ""))
    (decode body :: Maybe BookmarkJSON)

respondWithMaybeBookmark :: Int -> Key Bookmark -> Maybe Bookmark -> Snap ()
respondWithMaybeBookmark code bookmarkIdKey maybeBookmark =
  case maybeBookmark of
    Nothing ->
      writeBS ("{\"error\": \"Not found.\"}" :: Data.ByteString.ByteString)
    Just bookmark -> respondWithBookmark code bookmarkIdKey bookmark

respondWithBookmark :: Int -> Key Bookmark -> Bookmark -> Snap ()
respondWithBookmark code bookmarkIdKey bookmark = do
  modifyResponse $ setResponseCode code
  writeLBS $ bookmarkAsJSONLBS bookmarkIdKey bookmark
